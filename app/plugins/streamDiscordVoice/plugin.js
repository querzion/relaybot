const BasicPlugin = use('classes/BasicPlugin');
const {Readable} = require('stream');
const SILENCE_FRAME = Buffer.from([0xF8, 0xFF, 0xFE]);

/**
 * creates a silent file buffer, used for a workaround
 * https://github.com/discordjs/discord.js/issues/2929
 */
class Silence extends Readable {
  /**
   * pushes a single silent frame into the stream
   */
  _read() {
    this.push(SILENCE_FRAME);
    this.destroy();
  }
}

/**
 * @alias Plugin.streamDiscordVoice
 * @extends _.BasicPlugin
 */
class Plugin extends BasicPlugin {
 /**
   * runs the plugin code when called
   */
  init() {
    Promise.all([
      this._auriga.loadService('discord'),
      this._auriga.loadService('webLocalServer')
    ]).then((requirements) => {
      this.discord        = requirements[0];
      this.web            = requirements[1];
      this.reconnectTimer = 3000;

      this.registerRoutes()
      .then(this.joinVoiceLobbies())
      .then(this.watchVoiceEvents())
      .catch(this.warn);

      this.register();
    });
  }

  /**
   * Creates routes within the webLocalServer
   * @return {promise} resolves on success, rejects on errors
   */
  registerRoutes() {
    return new Promise((resolve, reject) => {
      this.web.socketio.on('connection', (socket) => {
        // let address   = socket.request.connection.remoteAddress;
        let service   = socket.request._query['service'];
        let channelID = socket.request._query['channel'];

        if (service !== 'voice_chat_monitor') {
          return;
        }

        // check if the channel is one we can connect to
        let channel = this.discord.bot.channels.cache.get(channelID);
        if (!channel || channel.type !== 'voice') {
          return;
        }

        // checks complete
        let room = `${service}:${channelID}`;
        socket.join(room);

        socket.on('roll-call', () => {
          this.rollCall(channel.guild.id).then((users) => {
            let packet = {
              type: 'roll-call',
              users: users
            };

            this.broadcastToSocket(channelID, packet);
          }).catch(this.warn);
        });

        socket.emit('connection', {});
      });

      this.webAssets = this.web.registerStatic(`${__dirname}/assets`, this);
      this.web.server.get('/widgets/voice/:channel_id', (req, res) => {
        res.render(`${__dirname}/views/voice_chat_monitor.twig`, {
          config: {
            channel: req.params.channel_id,
            text_color: '#ffffff',
            accent_color: '#cf404d',
            port: this.web._settings.port,
            fallbackAvatar: `${this.webAssets}/fallback.png`
          }
        });
      });
      resolve();
    });
  }

  /**
   * Listen to voice channel events
   * NOTE: Discord does not officially support receiving these events.
   */
  watchVoiceEvents() {
    this.discord.bot.on('guildMemberSpeaking', (member, state) => {
      let speaking = (
        state
        && state.bitfield
        && state.bitfield === 1
      ) ? true : false;

      let bot            = this.discord.bot;
      let connection     = bot.voice.connections.get(member.guild.id);
      let voiceChannelID = connection.channel.id;

      // send the event through the websocket
      this.broadcastToSocket(voiceChannelID, {
        type: 'speaking',
        speaking: speaking,
        user: {
          id:     member.user.id,
          name:   member.user.nickname || member.user.username,
          avatar: member.user.avatar || false
        }
      });
    });

    this.discord.bot.on('voiceStateUpdate', (oldSate, newState) => {
      let bot          = this.discord.bot;
      let connection   = bot.voice.connections.get(newState.guild.id);
      let voiceMember  = bot.users.cache.get(newState.id);

      if (!connection) {
        return;
      }

      let botChannelID = connection.channel.id;
      let oldChannel   = bot.channels.cache.get(oldSate.channelID);
      let newChannel   = bot.channels.cache.get(newState.channelID);

      // dispatch appropriate event through websockets
      let packet = {
        user: {
          id:     voiceMember.id,
          name:   voiceMember.nickname || voiceMember.username,
          avatar: voiceMember.avatar || false
        }
      };

      let send = true;
      if (!voiceMember.bot) {
        if (
          newChannel && oldChannel &&
          newChannel.id === oldChannel.id &&
          newChannel.id === botChannelID
        ) {
          // muted / deafened / whatever other event. Not interested
          send = false;
        } else if (newChannel && newChannel.id === botChannelID) {
          packet.type = 'join';
        } else if (oldChannel && oldChannel.id === botChannelID) {
          packet.type = 'leave';
        }
        if (send) {
          this.broadcastToSocket(botChannelID, packet);
        }
      }
    });
  }

  /**
   * broadcasts voice events through websockets
   * @param {string} socketRoom the socket.io room to send to
   * @param {object} packet an object containing event data
   */
  broadcastToSocket(socketRoom, packet) {
    this.web.socketio.to(`voice_chat_monitor:${socketRoom}`).emit('alert', packet);
  }

  /**
   * joins all voice lobbies registered in the config
   * @return {promise} resolves on successful join, rejects on errors
   */
  joinVoiceLobbies() {
    return new Promise((resolve, reject) => {
      const lobbies = this._settings.lobbies;
      if (lobbies && lobbies.length > 0) {
        lobbies.forEach((lobby) => {
          const channel = this.discord.bot.channels.cache.get(lobby);
          if (!channel || !channel.join) {
            reject(`Channel ID ${lobby} from config couldn't be found.`);
          }

          /**
           * Joins a channel, and ensures it reconnects automatically
           * @param {boolean} isReconnect states if reconnecting
           */
          let joinForever = (isReconnect = false) => {
            channel.join()
            .then((connection) => {
              let connectMsg = (isReconnect) ? 'Reconnected' : 'Connected';
              this.info(`${connectMsg} to lobby '${channel.name}' on '${channel.guild.name}'`);
              // patch: bot has to speak before it can listen
              connection.play(new Silence(), {type: 'opus'});
              connection.on('disconnect', (msg) => {
                // automatically reconnect
                this.warn(`Disconnected from lobby '${channel.name}' on '${channel.guild.name}'`);
                setTimeout(() => {
                  joinForever(true);
                }, this.reconnectTimer);
              });
              connection.on('error', (err) => {
                this.error(`Error connecting to lobby '${channel.name}' on '${channel.guild.name}': ${err}`);
              });
              resolve();
            }).catch((err) => {
              this.error(err);
              reject(err);
            });
          };

          joinForever();
        });
      } else {
        reject('No voice lobbies defined in settings');
      }
    });
  }

  /**
   * does all reconnection logic
   * @param {obj} channel the discordjs channel
   */
  doReconnects(channel) {
    channel.join().then((connection) => {
      const serverID = connection.channel.guild.id;
      this.rollCall(serverID).then((users) => {
        let packet = {
          type: 'roll-call',
          users: users
        };

        this.broadcastToSocket(channel.id, packet);
      }).catch(this.warn);
    }).catch((error) => {
      this.warn(`Couldn't connect to channel ${lobby} from config. Error: ${error}`);
    });
  }

  /**
   * Acquires a list of users in a given voice chat channel and brodacasts it
   * @param {string} serverID the server to roll call against
   * @return {promise} resolves with a list of users, rejects on errors
   */
  rollCall(serverID) {
    return new Promise((resolve, reject) => {
      let bot          = this.discord.bot;
      let connection   = bot.voice.connections.get(serverID);

      let users = connection.channel.members
      .filter(x => !x.user.bot)
      .map((x) => {
        let user = {};
        user.id = x.id;
        user.name = x.nickname || x.user.username;
        user.avatar = x.user.avatar;

        return user;
      });

      resolve(users);
    });
  }
};

module.exports = Plugin;
