/**
 * @file auriga.io
 * @author Frosthaven @thefrosthaven
 * @description auriga.io is a platform agnostic community bot that provides
 *              users with commands that they can run. Initially built
 *              for support with twitch chat and discord chat, auriga.io
 *              can take you as far as you want it to with its extendable
 *              class-inheritance based modular design and tight
 *              permission controls.
 */

(() => {

const fs           = require('fs');
const AurigaClient = require('./src/AurigaClient');
const yaml         = require('node-yaml');
const chalk        = require('chalk');
const yamlPath     = './app/config.yaml';

if (fs.existsSync(yamlPath)) {
  // Config File Found!
  config       = yaml.readSync(yamlPath);
  const auriga = new AurigaClient(config);
  auriga.log(chalk.magenta('\n******************************\nAURIGA PLATFORM STARTING...\n******************************'));
  auriga.on('ready', () => {
    auriga.log(chalk.magenta('\n******************************\nAURIGA PLATFORM LOADED!\n******************************'));
  });
} else {
  // No Config
  console.log(`ERROR: COULD NOT FIND ${yamlPath}.`);
}

})();
