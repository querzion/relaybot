/**
 * Class representing a rate limited queue
 * @alias _.RateLimitedQueue
 */
class RateLimitedQueue {
  /**
   * Creates a rate limited queue of units by which actions can be performed on
   * @param {object} params a group of parameters used to create the queue
   * @param {number} params.units how many units we are handling per a given time window
   * @param {number} params.per the time window in milliseconds for which units are handled
   * @param {function} params.action a handler that does something when a unit is ready
   */
  constructor(params) {
    // provided parameters
    this.units  = params.units;
    this.per    = params.per;
    this.action = params.action;

    // other parameters
    this.queue = [];
    this.task  = false;
    this.rate  = Math.ceil(this.per / this.units);
  }

  /**
   * Starts the queue if it isn't already running
   */
  start() {
    if (!this.task) {
      this.task = setInterval(() => {
        if (this.queue.length > 0 && typeof this.action === 'function') {
          let unit = this.queue.shift();
          this.action(unit);
        } else if (this.queue.length === 0) {
          this.stop();
        }
      }, this.rate);
    }
  }

  /**
   * Stops the queue
   */
  stop() {
    clearInterval(this.task);
    this.task = false;
  }

  /**
   * Adds something to the queue for processing, and attempts to start it
   * @param {*} unit anything that can be added to an array
   */
  add(unit) {
    this.queue.push(unit);
    this.start();
  }
};

module.exports = RateLimitedQueue;
