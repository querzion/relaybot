const fs   = require('fs');
const path = require('path');

/**
 * class representing a file directory auto loader
 * @alias _.AutoLoader
 */
class AutoLoader {
  /**
   * Creates a file loader that loads files within a directory as code
   * @param {object} params a group of parameters used to create the loader
   * @param {string} params.fileDir the file directory to load code from
   * @param {string} params.fileName the name of a valid .js and .json file to look for
   * @param {object} params.container the parent container that loaded files will have access to
   */
  constructor(params) {
    // provided params
    this.fileDir   = params.fileDir;
    this.fileName  = params.fileName;
    this.container = params.auriga;

    // other params
    this.collection = {};
  }

  /**
   * loads files into a collection that can be used elsewhere
   * @return {promise} resolves with the collection, rejects on errors
   */
  load() {
    return new Promise((resolve, reject) => {
      fs.readdir(this.fileDir, (err, items) => {
        if (err) {
          reject(err);
        } else {
          let total     = items.length;
          let processed = 0;
          items.forEach((dir) => {
            const subDir = path.normalize(`${this.fileDir}/${dir}`);
            if (!fs.lstatSync(subDir).isDirectory()) {
              resolve(false);
            }
            fs.readdir(subDir, (err, items) => {
              if (err) {
                reject(err);
              } else {
                if (items.includes(`${this.fileName}.js`)) {
                  const entryName = `${dir}`;
                  if (this.container.config[`${this.fileName}s`]) {
                    const settings  = this.container.config[`${this.fileName}s`][entryName];

                    if (settings && settings.enabled) {
                      const code      = require(path.normalize(`${this.fileDir}/${dir}/${this.fileName}.js`));
                      this.collection[entryName]          = {};
                      this.collection[entryName]._settings = settings;
                      this.collection[entryName].Class    = code;
                    }
                  }
                }
              }

              processed++;
              if (total === processed) {
                resolve(this.collection);
              }
            });
          });
        }
      });
    });
  }
}

module.exports = AutoLoader;
